//
//  Article.swift
//  This Just In
//
//  Created by Tom Holmes on 04/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation


struct RootObject: Codable {
    var articles: [Article]
}

struct Article: Codable {
    var source: Source
    var title: String
    var description: String?
    var url: URL
    var urlToImage: URL?
    var publishedAt: String
    var content: String?
}

struct Source: Codable {
    var name: String
}
