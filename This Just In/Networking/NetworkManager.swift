//
//  NetworkManager.swift
//  This Just In
//
//  Created by Tom Holmes on 04/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation

protocol NetworkMangerDelegate {
    func didUpdateHeadlines(_ networkManager: NetworkManager, headlines: HeadlinesModel)
    func didFailWithError(error: Error)
}

class NetworkManager {
    
    var root: RootObject?
    
    var delegate: NetworkMangerDelegate?
    
    func fetchTopHealines(for country: String) {
        var comps = URLComponents()
        
        comps.scheme = "https"
        comps.host = "newsapi.org"
        comps.path = "/v2/top-headlines"
        comps.queryItems = [URLQueryItem(name: "country", value: country),
                            URLQueryItem(name: "apiKey", value: K.Networking.apiKey)
        ]
        
        getTopUKResults(with: comps)
    }
    
    
    func getTopUKResults(with urlComps: URLComponents) {
        if let url = urlComps.url {
            
            print(url)
            
            let session = URLSession(configuration: .default)
            
            let task = session.dataTask(with: url) { (data, response, error) in
                
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                
                if let safeData = data {
                    if let headlines = self.parseJSON(safeData) {
                        self.delegate?.didUpdateHeadlines(self, headlines: headlines)
                    }
                }
                
            }
            
            task.resume()
        }
    }
    
    
    func parseJSON(_ data: Data) -> HeadlinesModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(RootObject.self, from: data)
            let articles = decodedData.articles
            
            let results = HeadlinesModel(articles: articles)
            
            return results
            
        } catch {
            print(error)
            return nil
        }
        
    }
    
}

