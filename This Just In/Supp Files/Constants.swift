//
//  Constants.swift
//  This Just In
//
//  Created by Tom Holmes on 05/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation

struct K {
    
    struct Networking {
        static let apiKey = "154a17140fb849cfa49c30da50944bff"
    }
    
    struct DateAndTime {
        static let apiFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        static let appFormat = "dd-MM-yyyy, HH:mm"
    }
    
    struct Segues {
        static let toArticle = "toArticle"
    }
    
    struct Cells {
        static let articleCellName = "articleCell"
        static let articleNibName = "ArticleCell"
        static let topCellName = "topCell"
        static let labelCellName = "labelCell"
        static let labelCellNibName = "LabelCell"
    }
    
    struct Assets {
        static let newsImagePlaceholder = "News"
    }
}
