//
//  ImageView.swift
//  This Just In
//
//  Created by Tom Holmes on 05/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit


class ImageCache {
    
    private init() {}
    static let shared = ImageCache()
    lazy var imageCache = NSCache<NSString, UIImage>()
    
}

extension UIImageView {
    
    //    let imageCache = NSCache<NSString, UIImage>()
    func imageFromServerURL(_ url: URL, placeHolder: UIImage?) {
        let cache = ImageCache.shared
        self.image = nil
        
        if let cachedImage = cache.imageCache.object(forKey: url.absoluteString as NSString) {
            self.image = cachedImage
            return
        }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            //print("RESPONSE FROM API: \(response)")
            if error != nil {
                print("ERROR LOADING IMAGES FROM URL: \(String(describing: error))")
                DispatchQueue.main.async {
                    self.image = placeHolder
                }
                return
            }
            DispatchQueue.global().async {
                if let data = data {
                    if let downloadedImage = UIImage(data: data) {
                        DispatchQueue.main.async {
                            cache.imageCache.setObject(downloadedImage, forKey: url.absoluteString as NSString)
                            self.image = downloadedImage
                        }
                    }
                }
            }
        }).resume()
    }
}
