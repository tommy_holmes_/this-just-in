//
//  ArticleViewController.swift
//  This Just In
//
//  Created by Tom Holmes on 21/08/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit
import WebKit

class ArticleViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    var articleWebPage: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = articleWebPage {
            webView.load(URLRequest(url: url))
        }
    }
    

    

}
