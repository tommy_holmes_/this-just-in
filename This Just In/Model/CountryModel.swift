//
//  CountryModel.swift
//  This Just In
//
//  Created by Tom Holmes on 15/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation

struct CountryModel {
    var countries = ["UK", "US", "France", "Germany"]
    let countryCodes = ["gb", "us", "fr", "de"]
}

//MARK: - Helpers
extension CountryModel {
    
}
