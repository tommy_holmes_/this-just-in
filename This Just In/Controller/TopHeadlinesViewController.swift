//
//  ViewController.swift
//  This Just In
//
//  Created by Tom Holmes on 03/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import UIKit

class TopHeadlinesViewController: UIViewController {
    
    @IBOutlet weak var countryCollectionView: UICollectionView!
    @IBOutlet weak var headlinesCollectionView: UICollectionView!
    
    var networkManager = NetworkManager()
    
    var model: HeadlinesModel!
    var countryModel: CountryModel!
    var chosenArticleURL: URL!
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cellSetUp()
        
        model = HeadlinesModel()
        countryModel = CountryModel()
        
        countryCollectionView.dataSource = self
        countryCollectionView.delegate = self
        countryCollectionView.isUserInteractionEnabled = true
        countryCollectionView.register(UINib(nibName: K.Cells.labelCellNibName, bundle: nil), forCellWithReuseIdentifier: K.Cells.labelCellName)
        
        headlinesCollectionView.dataSource = self
        headlinesCollectionView.delegate = self
        headlinesCollectionView.isUserInteractionEnabled = true
        headlinesCollectionView.register(UINib(nibName: K.Cells.articleNibName, bundle: nil), forCellWithReuseIdentifier: K.Cells.articleCellName)
        
        networkManager.delegate = self
        
        getResults(for: "gb")
    }
    
    
    func cellSetUp() {
        let articleWidth = view.frame.size.width
        let articleHeight = view.frame.size.height / 2.5
        let labelWidth = view.frame.size.width / 2
        let labelHeight = countryCollectionView.frame.size.height
        
        let headlinesCVLayout = headlinesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        headlinesCVLayout.itemSize = CGSize(width: articleWidth, height: articleHeight)
        
        let labelCVLayout = countryCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        labelCVLayout.itemSize = CGSize(width: labelWidth, height: labelHeight)
    }
    
    
    func getResults(for country: String) {
        networkManager.fetchTopHealines(for: country)
    }
    
    
    func formatDate(dateString: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = K.DateAndTime.apiFormat
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = K.DateAndTime.appFormat
        
        if let date = dateFormatterGet.date(from: dateString) {
            return dateFormatterPrint.string(from: date)
        } else {
            return "Date error"
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == K.Segues.toArticle {
            let destinationVC = segue.destination as? ArticleViewController
            destinationVC?.articleWebPage = chosenArticleURL
        }
    }
    
}


//MARK: - NetworkMangerDelegate
extension TopHeadlinesViewController: NetworkMangerDelegate {
    
    func didUpdateHeadlines(_ networkManager: NetworkManager, headlines: HeadlinesModel) {
        DispatchQueue.main.async {
            self.model.articles = headlines.articles
            self.headlinesCollectionView.reloadData()
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
}

//MARK: - UICollectionViewDataSource
extension TopHeadlinesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.countryCollectionView {
            return countryModel.countries.count
            
        } else {
            return model.numberOfArticles()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.countryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: K.Cells.labelCellName, for: indexPath) as! LabelCell
            
            let countryName = countryModel.countries[indexPath.row]
            
            cell.button.setTitle(countryName, for: .normal)
            
            return cell
            
        } else {
            let articleCell = collectionView.dequeueReusableCell(withReuseIdentifier: K.Cells.articleCellName, for: indexPath) as! ArticleCell
            
            let article = model.articles[indexPath.row]
            guard let imageURL = article.urlToImage else { return articleCell }
            
            articleCell.titleLabel.text = article.title
            articleCell.descriptionLabel.text = article.description
            articleCell.sourceLabel.text = article.source.name
            articleCell.timeLabel.text = formatDate(dateString: article.publishedAt)
            articleCell.imageView.imageFromServerURL(imageURL, placeHolder: UIImage(named: K.Assets.newsImagePlaceholder))
            
            return articleCell
        }
        
    }
    
    
}


//MARK: - UICollectionViewDelegate
extension TopHeadlinesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.countryCollectionView {
            
            DispatchQueue.main.async {
                let selecetedCountryCode = self.countryModel.countryCodes[indexPath.row]
                
                self.getResults(for: selecetedCountryCode)
                    
                self.headlinesCollectionView.reloadData()
            }
            
            
            
            
        } else {
            chosenArticleURL = model.articles[indexPath.row].url
            
            performSegue(withIdentifier: K.Segues.toArticle, sender: self)
        }
        
    }
    
}
