//
//  NewsModel.swift
//  This Just In
//
//  Created by Tom Holmes on 04/06/2020.
//  Copyright © 2020 Tom Holmes. All rights reserved.
//

import Foundation


struct HeadlinesModel {
    var articles: [Article] = []
}


//MARK: - Helpers
extension HeadlinesModel {
    
    func numberOfArticles() -> Int {
        let count = articles.count
        
        return count
    }
    
}
